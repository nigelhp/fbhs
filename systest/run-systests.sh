#!/bin/sh

project_dir="$(pwd)"
if [ "$#" -eq 1 ]
then
    project_dir="$1"
elif [ "$#" -gt 1 ]
then
    echo "Usage: run-systests.sh [<project_dir>]"
    exit 0
fi
echo "project_dir is [$project_dir]"

exe_path="$(find $project_dir -type 'f' -name 'fbhs' | head -n 1)"
echo "exe_path is [$exe_path]"
if [ -z "$exe_path" ]
then
    echo "Target executable not found.  Have you built the project?"
    exit 1
else
    shelltest "$project_dir/systest/fizzbuzz.test" -c -D"FIZZBUZZ=$exe_path"
fi
