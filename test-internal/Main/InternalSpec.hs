module Main.InternalSpec where

import Main.Internal
import Test.Hspec


spec :: Spec
spec = do
  describe "parseArgs" $ do
    it "should return failure when too few arguments are supplied" $
      parseArgs [] `shouldBe` Failure "Too few arguments"

    it "should return failure when too many arguments are supplied" $
      parseArgs ["1", "2"] `shouldBe` Failure "Too many arguments"

    it "should return failure when the single argument is not a numeric value" $
      parseArgs ["one"] `shouldBe` Failure "Argument must be a positive integer"

    it "should return failure when the single argument is negative" $
      parseArgs ["-1"] `shouldBe` Failure "Argument must be a positive integer"

    it "should return failure when the single argument is zero" $
      parseArgs ["0"] `shouldBe` Failure "Argument must be a positive integer"

    it "should return success when the single argument is positive" $
      parseArgs ["42"] `shouldBe` Success 42


  describe "process" $ do
    it "should apply the function only once when the numeric sequence is 1..1" $
      process show 1 `shouldBe` ["1"]

    it "should apply the function for each value in the sequence 1..n" $
      process show 10 `shouldBe` ["1","2","3","4","5","6","7","8","9","10"]
