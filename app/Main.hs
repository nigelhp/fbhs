module Main where

import FizzBuzz
import Main.Internal      (ParsedRequest(..), parseArgs, process, usage)
import System.Environment (getArgs)


main :: IO ()
main = do
  args <- getArgs
  case parseArgs args of
    Success n -> mapM_ putStrLn (process FizzBuzz.describe n)
    Failure m -> do
                   putStrLn $ "Invalid input: " ++ m ++ "\n"
                   printUsage


printUsage :: IO ()
printUsage = mapM_ putStrLn usage
