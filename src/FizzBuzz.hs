
module FizzBuzz (describe) where


{- Strictly speaking we just need an (Integral a, Show a),
   but that generates type warnings and Integer is assumed.

   Raising an error on a non-positive argument is the
   simplest thing to do here.  It is not a 'domain error'
   and we are not providing an API.  The program is invalid
   if this condition is encountered.
 -}
describe :: Integer -> String
describe n
  | n <= 0                 = error "Argument must be positive"
  | n `isMultipleOf` 3 &&
    n `isMultipleOf` 5     = "FizzBuzz"
  | n `isMultipleOf` 3     = "Fizz"
  | n `isMultipleOf` 5     = "Buzz"
  | otherwise              = show n  


isMultipleOf :: Integer -> Integer -> Bool
n `isMultipleOf` d = n `rem` d == 0
