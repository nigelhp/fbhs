module FizzBuzzSpec where

import Control.Exception (evaluate)
import Test.Hspec

import FizzBuzz


spec :: Spec
spec =
  Test.Hspec.describe "FizzBuzz" $ do
    context "when the number is not a multiple of 3 or 5 its decimal representation is output" $ do
      it "'1' is output for the number 1" $
        FizzBuzz.describe 1 `shouldBe` "1"
    
      it "'2' is output for the number 2" $
        FizzBuzz.describe 2 `shouldBe` "2"        

      it "'4' is output for the number 4" $
        FizzBuzz.describe 4 `shouldBe` "4"
        
    context "when the number is a multiple of 3 (and not a multiple of 5) 'Fizz' is output" $ do
      it "'Fizz' is output for the number 3" $
        FizzBuzz.describe 3 `shouldBe` "Fizz"

      it "'Fizz' is output for the number 6" $
        FizzBuzz.describe 6 `shouldBe` "Fizz"

      it "'Fizz' is output for the number 9" $
        FizzBuzz.describe 9 `shouldBe` "Fizz"
        
    context "when the number is a multiple of 5 (and not a multiple of 3) 'Buzz' is output" $ do
      it "'Buzz' is output for the number 5" $
        FizzBuzz.describe 5 `shouldBe` "Buzz"

      it "'Buzz' is output for the number 10" $
        FizzBuzz.describe 10 `shouldBe` "Buzz"

      it "'Buzz' is output for the number 20" $
        FizzBuzz.describe 20 `shouldBe` "Buzz"
        
    context "when the number is a multiple of both 3 and 5 'FizzBuzz' is output" $ do
      it "'FizzBuzz' is output for the number 15" $
        FizzBuzz.describe 15 `shouldBe` "FizzBuzz"

      it "'FizzBuzz' is output for the number 30" $
        FizzBuzz.describe 30 `shouldBe` "FizzBuzz"

      it "'FizzBuzz' is output for the number 45" $
        FizzBuzz.describe 45 `shouldBe` "FizzBuzz"

    context "an error is raised when the number is negative or zero" $ do
      it "raises an error for the number -1" $
        evaluate (FizzBuzz.describe (-1)) `shouldThrow` anyException

      it "raises an error for the number 0" $
        evaluate (FizzBuzz.describe 0) `shouldThrow` anyException
