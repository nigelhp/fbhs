module Main where 

import Control.Monad (unless)
import System.Exit
import Test.QuickCheck

import FizzBuzzProperties


main :: IO ()
main = do
  runPropertyWithExitOnFailure prop_MultipleOf3Not5IsFizz
  runPropertyWithExitOnFailure prop_MultipleOf5Not3IsBuzz
  runPropertyWithExitOnFailure prop_MultipleOf3And5IsFizzBuzz
  runPropertyWithExitOnFailure prop_NotMultipleOf3Or5IsItself

runPropertyWithExitOnFailure :: Testable prop => prop -> IO ()
runPropertyWithExitOnFailure prop = do
  result <- quickCheckResult prop
  unless (isSuccess result) exitFailure
