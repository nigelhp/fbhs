module FizzBuzzProperties (prop_MultipleOf3Not5IsFizz,
                           prop_MultipleOf5Not3IsBuzz,
                           prop_MultipleOf3And5IsFizzBuzz,
                           prop_NotMultipleOf3Or5IsItself)
where 

import Test.QuickCheck

import FizzBuzz


genMultipleOf3Not5 :: Gen Integer
genMultipleOf3Not5 = (arbitrary :: Gen Integer) `suchThat` (\n -> n > 0 &&
                                                                  n `rem` 3 == 0 &&
                                                                  n `rem` 5 /= 0)

genMultipleOf5Not3 :: Gen Integer
genMultipleOf5Not3 = (arbitrary :: Gen Integer) `suchThat` (\n -> n > 0 &&
                                                                  n `rem` 5 == 0 &&
                                                                  n `rem` 3 /= 0)

genMultipleOf3And5 :: Gen Integer
genMultipleOf3And5 = (arbitrary :: Gen Integer) `suchThat` (\n -> n > 0 &&
                                                                  n `rem` 3 == 0 &&
                                                                  n `rem` 5 == 0)

genNotMultipleOf3Or5 :: Gen Integer
genNotMultipleOf3Or5 = (arbitrary :: Gen Integer) `suchThat` (\n -> n > 0 &&
                                                                    n `rem` 3 /= 0 &&
                                                                    n `rem` 5 /= 0)

prop_MultipleOf3Not5IsFizz :: Property
prop_MultipleOf3Not5IsFizz = forAll genMultipleOf3Not5 (\n -> FizzBuzz.describe n == "Fizz")

prop_MultipleOf5Not3IsBuzz :: Property
prop_MultipleOf5Not3IsBuzz = forAll genMultipleOf5Not3 (\n -> FizzBuzz.describe n == "Buzz")

prop_MultipleOf3And5IsFizzBuzz :: Property
prop_MultipleOf3And5IsFizzBuzz = forAll genMultipleOf3And5 (\n -> FizzBuzz.describe n == "FizzBuzz")

prop_NotMultipleOf3Or5IsItself :: Property
prop_NotMultipleOf3Or5IsItself = forAll genNotMultipleOf3Or5 (\n -> FizzBuzz.describe n == show n)
