module FizzBuzzPropertiesSpec where

import Test.Hspec
import Test.Hspec.QuickCheck (prop)

import FizzBuzzProperties


spec :: Spec
spec =
  describe "FizzBuzz" $ do
    prop "describes multiples of 3 (that are not also multiples of 5) as 'Fizz'"
      prop_MultipleOf3Not5IsFizz

    prop "describes multiples of 5 (that are not also multiples of 3) as 'Buzz'"
      prop_MultipleOf5Not3IsBuzz

    prop "describes multiples of both 3 and 5 as 'FizzBuzz'"
      prop_MultipleOf3And5IsFizzBuzz

    prop "describes neither multiples of 3 or 5 as the number's decimal value"
      prop_NotMultipleOf3Or5IsItself


{- The above uses the 'prop' convenience function.
   The same result can be achieved by combining 'it' with 'property' as follows:

  describe "FizzBuzz" $ do
    it "describes multiples of 3 (that are not also multiples of 5) as 'Fizz'" $ property $
      prop_MultipleOf3Not5IsFizz

    it "describes multiples of 5 (that are not also multiples of 3) as 'Buzz'" $ property $
      prop_MultipleOf5Not3IsBuzz

    it "describes multiples of both 3 and 5 as 'FizzBuzz'" $ property $
      prop_MultipleOf3And5IsFizzBuzz

    it "describes neither multiples of 3 or 5 as the number's decimal value" $ property $
      prop_NotMultipleOf3Or5IsItself
 -}
