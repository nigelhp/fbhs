module Main.Internal (ParsedRequest(..), parseArgs, process, usage) where

import Text.Read (readMaybe)


data ParsedRequest =
    Success Integer
  | Failure String
  deriving (Eq, Show)

parseArgs :: [String] -> ParsedRequest
parseArgs []      = Failure "Too few arguments"
parseArgs (_:_:_) = Failure "Too many arguments" 
parseArgs (s:_)   =
  case toMaybeInteger s of
    Just n | n > 0 -> Success n
    _              -> Failure "Argument must be a positive integer"
  where toMaybeInteger = readMaybe :: String -> Maybe Integer


usage :: [String]
usage = [
  "Usage: fbhs n",
  "  where n >= 1"
  ]


process f n = [f i | i <- [1..n]]
process :: (Integer -> String) -> Integer -> [String]
