# 20.09
{ pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/cd63096d6d887d689543a0b97743d28995bc9bc3.tar.gz") {} }:

pkgs.mkShell {
  buildInputs = [
    pkgs.haskell.compiler.ghc865
    pkgs.haskellPackages.cabal-install
  ];

  NIX_PROJECT_NAME="fbhs";
}
